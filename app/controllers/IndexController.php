<?php

  //#################################################################
  
  use \P8R\Slim\ControllerBase as ControllerBase;

  //#################################################################
  
  /**
   * @renderer TwigRenderer 
   */
  class IndexController extends ControllerBase {

  	
    /**
     * @route       /index
     *
     * @scripts     ["/js/less.js"]
     * @styles-less ["/css-less/main.less"]
     */
    public function actionIndex() {
      
      $this->render( 'index/index.twig' );
    }
  	
    /**
     * @route       /hello/:name
     * #@route2      GET /hello/:name
     *
	   * @scripts     ["/js/jquery.js", "/js/less.js"]
	   * @styles-css  ["/css/main.css"]
	   * @styles-less ["/css-less/main.less"]
	   */
    public function actionHello( $name ) {
      //var_dump( $this );
      
      $obj = new stdClass();
      $obj->name = "Fujiko";
      
      $params = array();
      $params['name'] = "Piotrek";
      $params['obj'] = $obj;
      
      $this->render( 'index/hello.twig', $params );
    }
    
  }
  
  //---


?>