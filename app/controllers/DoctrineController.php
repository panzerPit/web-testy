<?php 
  //#################################################################
  
  use \P8R\Slim\ControllerBase as ControllerBase;

  //#################################################################
  
  /**
   * @renderer TwigRenderer 
   */
  class DoctrineController extends ControllerBase {
    
    /**
     * @route       /doctrine/index
     *
     * @scripts     ["/js/less.js"]
     * @styles-less ["/css-less/main.less"]
     */
    public function actionIndex() {
    	
    	$this->render( 'doctrine/index.twig' );
    	
    }

    //###############################################################
    
    /**
     * @route       /doctrine/odczyt
     *
     * @scripts     ["/js/less.js"]
     * @styles-less ["/css-less/main.less"]
     */
    public function actionOdczyt() {
    	
    	$data = array();
    	$data['test'] = 'testy....';
    	
      $this->render( 'doctrine/odczyt.twig', $data );
    }
    
    //###############################################################
    //###############################################################
  }
  
  //#################################################################

?>