<?php

  namespace P8R\Slim;

  //#################################################################
  
  /*
   * Domyślny renderer używany gdy nie został określony inny.
   */
  class EchoRenderer {
    public function render( &$arg1, $arg2 = null ) {
    	echo $arg1;
    }
  }
  
  //#################################################################
  
  /*
   * Renderer dla plików *.twig
   */
  class TwigRenderer {

    public function render( $file, $data = array() ) {
    
      $pos = strrpos( __FILE__, "app" );
      $templatesPath = substr( __FILE__, 0, $pos ) . "app/views";
      
      $loader = new \Twig_Loader_Filesystem( $templatesPath );
      
      $twig   = new \Twig_Environment($loader, array(
          'debug' => true,
          'cache' => ROOT_DIR . "/twig_views",
      ));
    
      $template = $twig->loadTemplate( $file );
    
      echo $template->render( $data );
    }
    
  }
  
  //#################################################################
  
?>