<?php
  namespace P8R\Slim;
  
  //#################################################################
  
  require 'ControllerBase.php';
  require 'ViewRenderer.php';

  //#################################################################
  
  class SlimApp {
  
    //###############################################################
    
    static private $app = null;
    static private $eM  = null;
    
    //###############################################################
    
    static public function getApp() { return self::$app; }
    static private function setApp( & $app ) {
      if (null != self::$app) return;
      self::$app = $app;
    }
  
    //###############################################################
    
    static private function setEM( &$em ) {
    	self::$eM = $em;
    }
    public function getEM() {
    	return self::$eM;
    }
    
    //###############################################################
  
    static public function Bootstrap( $initFunction = null ) {
      $app = new \Slim\Slim();
      self::setApp( $app );
      self::preprocessRoutes( $app );
      
      if (null != $initFunction && is_callable( $initFunction )) {
      	$initFunction( $app );
      }
      
      return $app;      
    }
    
    //###############################################################
    
    static function preprocessRoutes( & $app ) {
      $appDir = substr( __FILE__, 0, strrpos( __FILE__, "app", 2 ) );
      $controllersDir = $appDir . '/app/controllers';
      
      //var_dump( $controllersDir );
      
      //--- sprawdzamy czy istnieje
      if (!file_exists( $controllersDir ) || !is_readable( $controllersDir )) {
        throw new Exception( "Brak katalogu kontroler�w" );
        return false;
      }
      
      //--- przetworzeni kontroler�w
      
      $d = dir( $controllersDir );
      while (false !== ($file = $d->read())) {
        
        if (!preg_match( "/^.*\.php$/", $file )) continue;
        
        //print "Processing file: [{$controllersDir}]{$file}<br />";
        
        include_once $controllersDir . '/' . $file;
        
        $className = substr( $file, 0, strpos( $file, "." ) );
        
        //print "Class name: {$className} <br />";
        
        $class = new \ReflectionClass( $className );
        
        foreach ($class->getMethods() as $method) {
          
          if (!preg_match( "/^action/", $method->getName() )) continue;
          
          //print "Found action method: ". $method->getName() ."<br />";
          
          $reader = new \DocBlockReader\Reader( $className, $method->getName() );
          $route = trim( $reader->getParameter( 'route' ) );
          
          if ("" !== $route) {
            $methodName = $method->getName();
            $methodName = substr( $methodName, 6 );
            
            $actionDescriptor = $className. "::" . $methodName;
            //var_dump( $route, $actionDescriptor );
            //print "<br /><br />";
            
            $app->get( $route , $actionDescriptor );  
          }
        }
      }
      $d->close();
      
    }
    
    //###############################################################
  
  }
  
  //#################################################################

?>