<?php
  namespace P8R\Slim;

  //#################################################################
  
  class ControllerBase {
    protected $javaScripts      = null;
    protected $scriptBlocks     = null;
    protected $cssStyleSheets   = null;
    protected $lessStyleSheets  = null;
    
    protected $renderer         = null;
    private   $app              = null;
    
    //---
    protected function getApp() { return $this->app; }
    private   function setApp( & $app ) { $this->app = $app; }
    
    protected function getRequest() { return $this->app->request; }
    protected function getResponse() { return $this->app->response; }
    
    protected function getEM() { return $this->app->em; }
    
    //---
    
    static public function __callstatic( $name, $args ) {
      $bt = debug_backtrace();
      
      $controllerName = $bt[1]["class"];
      $methodName = "action" . $name;  //-- bez wczytywania sciezki z adnotacji np. "IndexController::Hello"
      
      //-- get controller class instance
      $rflClass = new \ReflectionClass( $controllerName );
      $instance   = $rflClass->newInstance();

      //-- process annotations
      $instance->processAnnotations( $instance, $methodName );
      
      $app = \P8R\Slim\SlimApp::getApp();
      $instance->setApp( $app );
      
      //-- call action method
      call_user_func_array( array($instance, $methodName), $args );
    }
    
    //---
    
    private function processAnnotations( &$instance, $methodName ) {
      $annotationMappings = array(
      	"scripts" => "javaScripts",
        "styles-css" => "cssStyleSheets",
        "styles-less" => "lessStyleSheets"
      );
      
      $prependURI = array( "scripts", "styles-css", "styles-less" );
      
      //--- controller annotations
      $readerCtrl = new \DocBlockReader\Reader( get_class( $instance ) );
      $annotationsCtrl = $readerCtrl->getParameters();
      
      if (isset( $annotationsCtrl['renderer'] )) {
        $renderer = null;
        $rendererName = $annotationsCtrl['renderer'];
        
        //-- obsługa renderera
        if ('none' != $rendererName) {
          try {
            //-- jeśli nie określono przestrzeni nazw, to dodajemy moją
            if ('\\' != $rendererName[0]) {
            	$rendererName = "\\P8R\\Slim\\" . $rendererName;
            }
            
            //-- utworzenie instancji klasy
            $rflClass = new \ReflectionClass( $rendererName );
            $renderer = $rflClass->newInstance();
          } catch (\Exception $ex) {
          	var_dump( "xxx" );
          	exit;
          }
        }
        
        if (null == $renderer) {
        	$renderer = new \P8R\Slim\EchoRenderer();
        }
        
      	$instance->renderer = $renderer; 
      }
        
      //--- action annotations
      $reader = new \DocBlockReader\Reader( get_class( $instance ), $methodName );
      $annotations = $reader->getParameters(); 
      
      $request = \P8R\Slim\SlimApp::getApp()->request;
      
      //-- 
      foreach ($annotationMappings as $annotation => $propertyName) {
      	if (array_key_exists( $annotation, $annotationMappings)) {
      	  if (!isset( $annotations[ $annotation ] )) continue;
      	  
          $annotationData = $annotations[ $annotation ];
          
          if (in_array( $annotation, $prependURI )) {
          	
            if (null == $annotationData) continue;
          	
            foreach ($annotationData as $indx => $data) {
              $annotationData[$indx] = $request->getRootUri() . $data;
            }
          }
          
          $instance->$propertyName = $annotationData;
      	}
      }
    }
    
    //---
    
    public function render( $arg1, $arg2 = array() ) {
      
      //--- default members transfer 
      $arg2['javaScripts']      = $this->javaScripts;
      $arg2['cssStyleSheets']   = $this->cssStyleSheets;
      $arg2['lessStyleSheets']  = $this->lessStyleSheets;
      
      //--- render
    	$this->renderer->render( $arg1, $arg2 );
    }
    
    //---
    
  }

  //############################################################################


?>