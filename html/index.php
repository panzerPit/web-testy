<?php
  require '../vendor/autoload.php';
  require '../app/lib/SlimApp.php';
  
  use \P8R\Slim\SlimApp as SlimApp;
  
  //---
  define( "ROOT_DIR", realpath( __DIR__ ."/.." ) );
  define( "APP_DIR", realpath( __DIR__ ."/../app" ) );
    
  //---
  $initialization = function ( & $app) {
    
    // ... kod inicjalizacyjny
    
  };
  
  //---
  
  SlimApp::Bootstrap( $initialization )->run(); 
  
?>
